import "bootstrap/dist/css/bootstrap.min.css";
import {BrowserRouter as Router, Route} from "react-router-dom";
import Navbar from "./Components/Navbar";
import Footer from "./Components/Footer";
import Map from "./views/Map";
import Datenschutz from "./views/Datenschutz";
import Impressum from "./views/Impressum";
import "./App.css";

/* 
  App encapsulates our entire website

    - Uses views with react-router-dom for seamless browsing
*/
function App() {
    return (
      <Router>
        <div className="root">
          <Navbar />
          <Route path="/" exact component={Map}/>
          <Route path="/Datenschutz" exact component={Datenschutz} />
          <Route path="/Impressum" exact component={Impressum} />
          <Footer />
        </div>
      </Router>
    );
}

export default App;

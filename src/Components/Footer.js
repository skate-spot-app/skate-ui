import React from "react";
import {Link} from "react-router-dom";
import "./Footer.css";

/* 
  Footer used on all views
*/
const Footer = () => {
    return (
        <div className="footer-sticky">
            <ul className="footer-link">
                <Link to="/Datenschutz">
                    <li>Datenschutz</li>
                </Link>

                <Link to="/Impressum">
                    <li> Impressum</li>
                </Link>
                <li>
                    &copy;{new Date().getFullYear()} Skate Community GmbH |
                </li>
            </ul>
        </div>
    );
};
export default Footer;

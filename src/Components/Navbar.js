import React from "react";
import {Link} from "react-router-dom";
import "./Navbar.css";

/* 
  Navbar used on all views
*/
const Navbar = () => {
    return (
        <nav className="navbar navbar-dark" style={{backgroundColor: "#c4d1c5", borderBottom: "1px solid #081709", display: "flex", backgroundImage: "url(img/header-image.png)"}}>
            <div className="navbar-brand" style={{ margin: 0 }} href="/">
                <img
                    src="/img/skate-app-icon.png"
                    className="navbar-brand"
                    width="70"
                    height="70"
                    alt="skate-app-icon"
                />
            </div>
            <a style={{ textDecoration: "none", color: "#ffffff" }} href="/">
                <h1 style={{ margin: 0 }}>Skate Community</h1>
            </a>
            <ul style={{display: "flex", justifyContent: "space-between", margin: "0 1em", padding: 0, marginLeft: "auto"}}>
                <Link to="/" className="Link" style={{color: "#ffffff"}}>
                    <li>
                        Home
                    </li>
                </Link>
            </ul>
        </nav>
    );
};
export default Navbar;

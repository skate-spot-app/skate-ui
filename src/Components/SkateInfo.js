    import React from "react";
    import "./SkateInfo.css";

    const SkateInfo = (props) => {
        return (
            <div className="container_skateinfo">
                {props.image &&
                <img src={props.image} alt="skate spot"  className="skate-img"/>
                }
                <h3>{props.title}</h3>
                {props.address &&
                <div className="Address">
                    <i class="fas fa-map-marker-alt" style={{display: "inline", paddingRight: "0.5em"}}></i>
                    <p style={{display: "inline"}}>{props.address}</p>
                </div>
                }
                <h5>Beschreibung</h5>
                <p>{props.description}</p>
                {props.openingTimes &&
                <div>
                <h5>Öffnungszeiten</h5>
                <p>{props.openingTimes}</p>
                </div>
                }
                <p>erstellt am: {props.createDate}</p>
            </div>
        )
    }

    export default SkateInfo;

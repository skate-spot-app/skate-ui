import React, {useState, useEffect} from 'react'
import {Button, Col, Container, Form, Modal, Row} from "react-bootstrap";
import axios from "axios";
import "./SkateSpotForm.css";
import "../global.css";


const SkateSpotForm = (props) => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [address, setAddress] = useState("");
    const [startDay, setStartDay] = useState("");
    const [endDay, setEndDay] = useState("");
    const [startTime, setStartTime] = useState("");
    const [endTime, setEndTime] = useState("");
    const [selectedImage, setSelectedImage] = useState("");
    const [modalShow, setModalShow] = useState(props.show);

    const handleChangeFile = (e) => {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            localStorage.setItem('image', file)
            console.log(localStorage.getItem('image'))
            setSelectedImage(reader.result);
            props.addImage({"title": title, "image": reader.result})
        }
        reader.readAsDataURL(file)
    }

    const onSubmit = (e) => {
        e.preventDefault();

        let formTextData = new FormData();
        formTextData.append('title', title);
        formTextData.append('lng', props.lng);
        formTextData.append('lat', props.lat);
        formTextData.append('description', description);
        formTextData.append('address', address);
        if (startDay === "") {
            formTextData.append('openingTimes', "");
        } else {
            formTextData.append('openingTimes', `${startDay} - ${endDay}, ${startTime} - ${endTime}`);
        }

        axios({
            method: 'post',
            url: 'http://localhost:8080/add',
            data: formTextData
        }).then(() => {
            // File upload is not yet implemented in backend
            // props.addImage({"title": title, "image": selectedImage})
            // setModalShow(false)
            // props.onHide()
            window.location.reload(true)
        }).catch((e) => {
            console.log(e)
        })
    }

    useEffect(() => {
        setModalShow(props.show)
    }, [props.show])

    return (
        <Modal
            show={modalShow}
            onHide={() => {
                setModalShow(false)
                props.onHide()
            }}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
            <Form onSubmit={onSubmit} enctype="multipart/form-data">
                <Modal.Header closeButton>
                    <Modal.Title>Hinzufügen eines neuen Skate Spots</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container fluid>
                        {/* File upload is not yet implemented in backend
                        <Row>
                            <Col>
                                <img
                                    className="d-block w-100"
                                    src={selectedImage} alt="Skate Spot"/>
                                <div className="file-uploader">
                                    <input type="file" accept=".png" onChange={e =>
                                        handleChangeFile(e)}/>
                                </div>
                            </Col>
                        </Row>
                        */}
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Titel</Form.Label>
                                    <Form.Control required type="text" placeholder="Titel des Skatespots"
                                                  onChange={e => setTitle(e.target.value)}/>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Beschreibung</Form.Label>
                                    <Form.Control required as="textarea" rows={2} placeholder="Beschreibung des Skatespots"
                                                  onChange={e => setDescription(e.target.value)}/>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group controlId="formGridAddress1">
                                    <Form.Label>Address</Form.Label>
                                    <Form.Control required type="text" placeholder="Adresse des Skatespots"
                                                  onChange={e => setAddress(e.target.value)}/>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Form.Group>
                                <Col>
                                    <Form.Label>Öffnungszeiten</Form.Label>
                                    <Row>
                                        <div className="opening-times-form-day-col">
                                            <Col md={"auto"}>
                                                <p>Tage:</p>
                                            </Col>
                                            <Col md={"auto"}>
                                                <Form.Control as="select" onChange={e => setStartDay(e.target.value)}>
                                                    <option value="" selected disabled>Von</option>
                                                    <option>Montag</option>
                                                    <option>Dienstag</option>
                                                    <option>Mittwoch</option>
                                                    <option>Donnerstag</option>
                                                    <option>Freitag</option>
                                                    <option>Samstag</option>
                                                    <option>Sonntag</option>
                                                </Form.Control>
                                            </Col>
                                            <p>-</p>
                                            <Col md={"auto"}>
                                                <Form.Control as="select" onChange={e => setEndDay(e.target.value)}>
                                                    <option value="" selected disabled>Bis</option>
                                                    <option>-</option>
                                                    <option>Montag</option>
                                                    <option>Dienstag</option>
                                                    <option>Mittwoch</option>
                                                    <option>Donnerstag</option>
                                                    <option>Freitag</option>
                                                    <option>Samstag</option>
                                                    <option>Sonntag</option>
                                                </Form.Control>
                                            </Col>
                                        </div>
                                        <div className="opening-times-form-time-col">
                                            <Col md={"auto"}>
                                                <p>Uhrzeit:</p>
                                            </Col>
                                            <Col md={"auto"}>
                                                <Form.Control type="time" onChange={e => setStartTime(e.target.value)}/>
                                            </Col>

                                            <p>-</p>

                                            <Col md={"auto"}>
                                                <Form.Control type="time" onChange={e => setEndTime(e.target.value)}/>
                                            </Col>

                                        </div>
                                    </Row>
                                </Col>
                            </Form.Group>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer> {
                    <Button type="submit" className="custom-btn-color"
                            variant="default">
                        Skate Spot hinzufügen
                    </Button>
                }
                </Modal.Footer>
            </Form>
        </Modal>
    );
}

export default SkateSpotForm;
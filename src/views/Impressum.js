import React, {Component} from "react";
/* 
  Impressum view
*/
export default class Impressum extends Component {
    render() {
        return (
            <div style={{ flexGrow: 1, margin: "1em 5em" }}>
                <div>
                    <h1>Impressum</h1>
                    <p> Skate Community GmbH <br/>
                        Skate Straße 1 <br/>
                        28195 Bremen <br/>
                        <br/>
                        Telefon: 0176 / 10101010 <br/>
                        E-Mail: Skatesociety@sc.com <br/>
                    </p>
                </div>
            </div>
        );
    }
}

import React, {useEffect, useState} from "react";
import L from "leaflet";
import {MapContainer, Marker, Popup, TileLayer} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import "./Map.css";
import SkateInfo from "../Components/SkateInfo";
import {Button} from "react-bootstrap";
import SkateSpotForm from "../Components/SkateSpotForm";
import "./Map.css";
import "../global.css";



const DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;

/*
  Map view responsible for showing an interactive map.

    - Uses leaflet, react-leaflet and openstreetmap.
*/
const Map = () => {
    const [skateSpots, setSkateSpots] = useState([])
    const [lat, setLat] = useState(null)
    const [lng, setLng] = useState(null)
    const [modalShow, setModalShow] = useState(false);
    const [images, setImages] = useState([])

    const centerPosition = [53.08, 8.805]
    let marker = L.marker(null, icon)


    const getSkateSpots = () => {
        fetch("http://localhost:8080/all")
            .then(res => res.json())
            .then(result => {
                result.map((spot, index) => {
                    spot.image = images[index]
                    return spot
                })
                setSkateSpots(result)
            })
    }

    useEffect(() => {
            getSkateSpots()
    }, [])

    useEffect(() => {}, [modalShow])

    const addImage = (imageObject) => {
        setSkateSpots((skateSpots) => {
            skateSpots.forEach(spot => {
                if (spot.title === imageObject.title) {
                    spot.image = imageObject.image
                }
            })
            return skateSpots
        })
    }

    return (
        <div style={{height: "100%"}}>
            <MapContainer
                center={centerPosition}
                scrollWheelZoom={true}
                zoom={12}
                style={{height: "100%"}}
                whenReady={(map) => {
                    map.target.on("click", e => {
                        const {lat, lng} = e.latlng


                        marker.setLatLng(e.latlng)
                        setLat(lat)
                        setLng(lng)
                        marker.addTo(map.target)
                    })
                }}
            >
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {skateSpots.map(spot => (
                    <Marker key={spot.id} position={[spot.lat, spot.lng]}>
                        <Popup>
                            <SkateInfo title={spot.title} description={spot.description} image={spot.image}
                            address={spot.address} openingTimes={spot.openingTimes} createDate={spot.createDate} />
                        </Popup>
                    </Marker>
                ))}
            </MapContainer>
            {lat !== null &&
            <Button size="lg"
                    style={{zIndex: "999", position: "absolute", bottom: "4em", left: "2em"}}
                    className="custom-btn-color"
                    variant="default"
                    onClick={() => setModalShow(true)}>
                Skate Spot hinzufügen
            </Button>
            }
            <SkateSpotForm show={modalShow}
                           onHide={() => setModalShow(false)}
                           addImage={imgObject => addImage(imgObject)}
                           lng={lng}
                           lat={lat}
                           getSkateSpots={getSkateSpots}
            />
        </div>
    );
}

export default Map;
